#!/usr/bin/env python3
#/****************************************************************************
# Convex polygon area coverage planner
# Copyright (c) 2016-2020, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# University of Southern Denmark
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************/
"""
2016-09-12 KJ First version
2020-03-19 KJ Python3 compatible
"""

# load generic modules
from math import pi
import numpy as np
import matplotlib.pyplot as plt
from pylab import *

# load application modules
from waypoint_import import waypoint_import
from transverse_mercator import tranmerc
from area_coverage import area_coverage_planner
from exportkml import kmlclass

# define input
poly_file = 'waypoints_field.txt'
start_lat = 55.38174 # decimal degrees
start_lon = 10.34132 # decimal degrees
leg_orient = 0.0 # [degree]
drone_yaw = 90.0 # [degree]
first_turn_left = True
leg_width = 25.0 # [m]
height_type = 0
height_coverage = 20.0 # [m]
leg_velocity = 10.0 # [m/s]
wpt_loiter_time = 3.0 # [s]
grid_width = 5 # [m]

# qgc parameters
qgc_dunno = 0
if height_type != 0:
	qgc_frame_takeoff = 0 # 0 = abs 3 = relative
	qgc_frame_wpt = 0 # 0 = abs 3 = relative
else:
	qgc_frame_takeoff = 3 # 0 = abs 3 = relative
	qgc_frame_wpt = 3 # 0 = abs 3 = relative
qgc_poi_heading = drone_yaw
qgc_vert_vmax = 1.0
qgc_radius = 4.0
qgc_loiter = wpt_loiter_time
qgc_hori_vmax = leg_velocity
qgc_yaw = drone_yaw
qgc_dunno2 = 1

MAV_CMD_NAV_WAYPOINT = 16
MAV_CMD_NAV_TAKEOFF = 22

# WGS-84 defines
wgs84_a = 6378137.0 # WGS84 semi-major axis of ellipsoid [m]
wgs84_f = 1/298.257223563 # WGS84 flattening of ellipsoid
deg2rad = pi/180.0
rad2deg = 180.0/pi

# UTM defines
utm_false_easting = 500000.0
utm_scale_factor = 0.9996
utm_origin_latitude = 0.0

# UTM zone defines
false_northing = 0.0
central_meridian = 9.0

# import waypoint list
wi = waypoint_import()
polyll = wi.import_lat_lon_list (poly_file)

# convert latlon to transverse mercator
tm = tranmerc()
tm.set_params (wgs84_a, wgs84_f, utm_origin_latitude, central_meridian*deg2rad, utm_false_easting, false_northing, utm_scale_factor)
polytm = []
for i in range(len(polyll)):
	(e, n) = tm.geodetic_to_tranmerc(polyll[i][0]*deg2rad, polyll[i][1]*deg2rad)
	polytm.append([e,n])
(start_e, start_n) = tm.geodetic_to_tranmerc(start_lat*deg2rad, start_lon*deg2rad)

# scale to relative coordinates
e_offset = 1e9
n_offset = 1e9
for i in range(len(polytm)):
	if polytm[i][0] < e_offset:
		e_offset = polytm[i][0]
	if polytm[i][1] < n_offset:
		n_offset = polytm[i][1]
poly = []
for i in range(len(polytm)):
	poly.append([polytm[i][0]-e_offset, polytm[i][1]-n_offset])
start_e_rel = start_e - e_offset
start_n_rel = start_n - n_offset

# area coverage planning
acp = area_coverage_planner()
print ('Area coverage route planner algorithm')
area_size = acp.poly_area(poly)
print ('Area size: %.0f m^2  %.3f km^2 %.1f Ha' % (area_size, area_size/1000000.0, area_size/10000.0))

# route planning
(rte, grid, emin, emax, nmin, nmax) = acp.route_planner (poly, [start_e_rel, start_n_rel], leg_orient*deg2rad, grid_width, leg_width, first_turn_left)
print ('Route waypoints: %d' % len(rte))
route_length = acp.route_length(rte)
print ('Route length: %.0f m' % route_length)
route_seconds = route_length/leg_velocity
route_seconds += len(rte)*wpt_loiter_time
route_minutes = int(route_seconds / 60.0)
route_seconds -= route_minutes*60
print ('Route duration: %.0f min %.0f sec' % (route_minutes, route_seconds))


# scale back to transverse mercator
rte_tm = []
for i in range(len(rte)):
	rte_tm.append([rte[i][0]+e_offset, rte[i][1]+n_offset])

# convert transverse mercator to latitude and longitude
rte_ll = []
for i in range(len(rte_tm)):
	(lat, lon) = tm.tranmerc_to_geodetic(rte_tm[i][0], rte_tm[i][1])
	rte_ll.append([lat*rad2deg,lon*rad2deg])

rte_alt = []

# determine coverage height for each waypoint
if height_type == 0: # relative to launch height
	print ('Height relative to launch: %.1f m' % height_coverage)
	rte_alt.append (height_coverage)
	for i in range(1, len(rte_tm)):
		rte_alt.append (0.0)

elif height_type == 1: #absolute above MSL
	print ('Height above MSL: %.1f m' % height_coverage)
	for i in range(len(rte_tm)):
		rte_alt.append (height_coverage)

# export to QGC waypoint list
f = open ('waypoints.txt', 'w')
f.write ('QGC WPL 120\n')
for i in range(len(rte_ll)):
	f.write ('%d\t%d\t%d\t%d\t%.2f\t%.0f\t%.2f\t%.2f\t%.8f\t%.8f\t%.3f\t%d\n' % (i+1, qgc_dunno, qgc_frame_wpt, MAV_CMD_NAV_WAYPOINT, qgc_radius, qgc_loiter*1000, qgc_hori_vmax, qgc_yaw, rte_ll[i][0], rte_ll[i][1], rte_alt[i], qgc_dunno2))

f.close

# export to CSV
f = open ('waypoints.csv', 'w')
for i in range(len(rte_ll)):
	f.write ('%.8f,%.8f,%.3f\n' % (rte_ll[i][0], rte_ll[i][1], rte_alt[i]))
f.close

# export to KML
kml = kmlclass()
kml.begin('waypoints.kml', 'Area coverage planner', '', 1.5)
kml.trksegbegin ('','','red','absolute')
for i in range(len(rte_ll)):
	kml.pt (rte_ll[i][0], rte_ll[i][1], rte_alt[i])
kml.trksegend()
kml.end()	

# plot result
plt.figure(1)
poly.append(poly[0]) # plot polygon
polyT = list(zip(*poly))
poly_plt = plot(polyT[0],polyT[1],'black')
grid_ok=[] # plot grid
grid_no=[]

for i in range (grid.shape[0]):
    for j in range (grid.shape[1]):
        if grid[i,j] == 1:
            grid_ok.append ([i*grid_width+emin, j*grid_width+nmin])
        else:
            grid_no.append ([i*grid_width+emin, j*grid_width+nmin])
grid_okT = list(zip(*grid_ok))
grid_noT = list(zip(*grid_no))
grid_ok_plt = plot(grid_okT[0],grid_okT[1],'g+',)
grid_no_plt = plot(grid_noT[0],grid_noT[1],'r+',)

rteT = list(zip(*rte))
rte_plt = plot(rteT[0],rteT[1],'blue')
title ('Route plan')
axis('equal')
xlabel('Easting [m]')
ylabel('Northing [m]')
plt.savefig ('waypoints_map.png')

