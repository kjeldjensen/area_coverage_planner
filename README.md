# area_coverage_planner

This is a simple area coverage planner designed to create a route plan for a drone overflying a field.

License:
The software has been released under a BSD 3-Clause license.

Input:
The planner takes as input the file waypoints field.txt describing the field boundaries in geographical coordinates. All other settings are specified at the top of the file route planner.py

Command line:
	python3 route_planner.py

Output:
waypoints.kml
- The route plan waypoints formatted as KML

waypoints.csv
- The route plan waypoints formatted as CSV

waypoints.txt
- The route plan waypoints formatted for QGroundControl import

waypoints_map.png
- A map of the field and route plan waypoints.

Limitations:
- The planner takes only convex areas.
- The planner only plans to one side depending on the first turn left parameter. Thus if you start in the middle of the area, the planner is not complete. 

Kjeld Jensen
University of Southern Denmark 
https://sdu.dk/uas
kjen@mmmi.sdu.dk
kj@kjen.dk

