#!/usr/bin/env python3
#/****************************************************************************
# Convex polygon area coverage planner
# Copyright (c) 2016-2020, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# University of Southern Denmark
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************/
"""
Local area coverage algorithm

Input
  Track defining area polygon
  Robot position
  Desired driving direction

Assumptions
  Polygon is convex
  Robot/drone can turn about its own center

2016-09-12 KJ First version
2020-03-19 KJ Python3 compatible
"""

# load modules
from math import sqrt, cos, sin, pi
import numpy as np

class area_coverage_planner():
	def __init__(self):
		pass

	# return the vector v rotated by theta
	def vec2d_rot (self, v, theta): 
		rot_x = v[0]*cos(theta) - v[1]*sin(theta)
		rot_y = v[0]*sin(theta) + v[1]*cos(theta)
		return ([rot_x, rot_y])

	def pt_in_poly(self, e,n,p):
		# Determine if a point lies within a polygon
		# http://www.ariel.com.au/a/python-point-int-poly.html
		l = len(p)
		inside = 0  
		p1e,p1n = p[0]
		for i in range(l+1):
			p2e,p2n = p[i % l]
			if n > min(p1n,p2n):
				if n <= max(p1n,p2n):
					if e <= max(p1e,p2e):
						if p1n != p2n:
							einters = (n-p1n)*(p2e-p1e)/(p2n-p1n)+p1e
						if p1e == p2e or e <= einters:
							inside = not inside
			p1e,p1n = p2e,p2n
		return inside

	def poly_area (self, poly):
		# Calculate poygon area size
		# http://en.wikipedia.org/wiki/Shoelace_formula
		n = len(poly)
		a = 0.
		for i in range(n):
			j = (i+1) % n
			a += poly[i][0]*poly[j][1] - poly[j][0]*poly[i][1]
		a = abs(a/2.)
		return a

	def route_planner (self, polygon, start, orient, grid_width, leg_width, first_turn_left):

		# rotate polygon
		poly = []
		for i in range(len(polygon)):
			poly.append(self.vec2d_rot([polygon[i][0],polygon[i][1]], orient))

		# rotate start
		start = self.vec2d_rot(start, orient)

		# find extreme points
		emax = emin = poly[0][0]
		nmax = nmin = poly[0][1]
		for i in poly:
			if emax < i[0]: emax = i[0]
			elif emin > i[0]: emin = i[0]
			if nmax < i[1]: nmax = i[1]
			elif nmin > i[1]: nmin = i[1]

		# create grid
		grid = np.zeros (((int) ((emax-emin)/grid_width+1),(int) ((nmax-nmin)/grid_width+1)))
		#print ('Grid cell grid_width %.2f m' % grid_width)
		#print 'Grid size:',grid.size
		#print 'Grid shape:',grid.shape

		# mark grid points inside polygon as traversible
		pts_in_poly = 0
		pin = []
		for i in range (grid.shape[0]):
			for j in range (grid.shape[1]):
				if self.pt_in_poly(i*grid_width+emin,j*grid_width+nmin,poly) == 1:
					grid[i,j] = 1
					pts_in_poly += 1
					pin.append ([i*grid_width+emin, j*grid_width+nmin])
		pin_lst = range(len(pin))

		#print 'Grid points in polygon:',pts_in_poly

		# find point nearest start
		start_pt_ = 0
		start_pt_min_dist = 1e9
		for i in pin_lst:
			dist_pt_start = sqrt((start[0]-pin[i][0])**2 + (start[1]-pin[i][1])**2)
			if dist_pt_start < start_pt_min_dist:
				start_pt_min_dist = dist_pt_start
				start_pt = i

		# generate route plan
		rte = []
		rte.append (pin[start_pt])
		finished = False
		current_col_e = pin[start_pt][0]
		next_is_top = True
		while finished == False:
			# find top of current grid column
			if next_is_top == True:	
				top_current_pt = len(rte)-1
				max_top = -1e9
				for i in pin_lst:
					if abs(pin[i][0]-current_col_e) < 0.001 and pin[i][1] > max_top:
						max_top = pin[i][1]
						top_current_pt = i
				if rte[-1][0] != pin[top_current_pt][0] or rte[-1][1] != pin[top_current_pt][1]:
					rte.append (pin[top_current_pt])

				# find top of this grid column
				if first_turn_left == True:
					current_col_e -= leg_width
				else:
					current_col_e += leg_width

				# goto top of next grid column
				max_top = -1e9
				for i in pin_lst:
					if abs(pin[i][0]-current_col_e) < 0.001 and pin[i][1] > max_top:
						max_top = pin[i][1]
						top_current_pt = i
				if max_top != -1e9:
					rte.append (pin[top_current_pt])

					# goto bottom of current grid column
					min_low = 1e9
					for i in pin_lst:
						if abs(pin[i][0]-current_col_e) < 0.001 and pin[i][1] < min_low:
							min_low = pin[i][1]
							bottom_current_pt = i
					if min_low != 1e9:
						rte.append (pin[bottom_current_pt])

					# goto bottom of next grid column
					if first_turn_left == True:
						current_col_e -= leg_width
					else:
						current_col_e += leg_width
					min_low = 1e9
					for i in pin_lst:
						if abs(pin[i][0]-current_col_e) < 0.001 and pin[i][1] < min_low:
							min_low = pin[i][1]
							bottom_current_pt = i
					if min_low != 1e9:
						rte.append (pin[bottom_current_pt])
					else:
						finished = True
				else:
					finished = True

		# rotate route
		rte_rot = []
		for i in range(len(rte)):
			rte_rot.append(self.vec2d_rot([rte[i][0],rte[i][1]], -orient))

		return (rte_rot, grid, emin, emax, nmin, nmax)

	def route_length (self, rte):
		dist = 0.0
		for i in range(len(rte)):
			if i > 0:
				dist += sqrt((prev[0]-rte[i][0])**2 + (prev[1]-rte[i][1])**2)
			prev = rte[i]
		return dist

